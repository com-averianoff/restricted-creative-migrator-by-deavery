# Restricted Creative migrator by deavery



## Getting started

- [ ] Download the jar file from the repository (artifacts folder). Create a folder and put the artifact in it.

![image.png](./images/image-1.png)

## Migration process

- [ ] Place the database file ```rc_database.db``` of the old version of the plugin.

![image-1.png](./images/image-2.png)

- [ ] Run with the command: 
```
java -jar <filename>.jar
```

![image-2.png](./images/image-3.png)

The resulting ```rc_migrated.db``` database must be renamed to ```rc_database.db``` (tables for storing blocks and inventories: ```rc_block```, ```rc_inventories```)

![image-4.png](./images/image-4.png)![image-3.png](./images/image-5.png)![image-6.png](./images/image-6.png)![image-7.png](./images/image-7.png)
