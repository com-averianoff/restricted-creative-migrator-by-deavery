package com.averianoff;

import com.averianoff.database.MigrationDB;
import com.averianoff.database.OriginalDB;
import com.averianoff.storage.MigrationStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RCMigrator {
    private static Logger logger = LoggerFactory.getLogger(RCMigrator.class);
    public static MigrationStorage MIGRATION_STORAGE = new MigrationStorage();
    public static String BLOCK_OWNER = "f66d517f-6043-300e-b9e4-ab171eda09a4";
    public static long CURRENT_TIME = System.currentTimeMillis();

    public static void main(String[] args) {;
        logger.info("RCMigrator by deavery");
        OriginalDB originalDB = new OriginalDB();
        originalDB.connect();
        originalDB.getBlocks();
        originalDB.getInventories();
        originalDB.disconnect();

        MigrationDB migrationDB = new MigrationDB();
        migrationDB.connect();
        migrationDB.createStructure();
        migrationDB.insertBlocks();
        migrationDB.insertInventories();
        migrationDB.disconnect();
    }
}