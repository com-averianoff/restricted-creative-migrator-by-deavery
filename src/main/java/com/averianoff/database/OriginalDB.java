package com.averianoff.database;

import com.averianoff.RCMigrator;
import com.averianoff.model.TranslatedBlockRow;
import com.averianoff.model.TranslatedInventoryRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class OriginalDB {

    private static Logger logger = LoggerFactory.getLogger(OriginalDB.class);
    private static String SELECT_ALL_BLOCKS = "select * from rc_blocks";
    private static String SELECT_ALL_INVENTORIES = "select * from rc_invs";
    private Connection connection;

    public OriginalDB() {
        this.connection = null;
    }

    public Connection connect() {
        logger.info("OriginalDB connecting...");
        connection = null;

        try {

            connection = DriverManager.getConnection("jdbc:sqlite:rc_database.db");
            logger.info("OriginalDB connected...");

        } catch(SQLException e) {
            logger.error("if the error message is \"out of memory\", it probably means no database file is found: "
                    + e.getMessage());
        }

        return connection;
    }

    public boolean getBlocks() {

        try {

            logger.info("Connecting to blocks table");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(120);

            ResultSet rs = statement.executeQuery(SELECT_ALL_BLOCKS);
            int rsCount = 0;
            while(rs.next()) {
                String rawOriginalBlockRow = rs.getString("block");
                TranslatedBlockRow translatedBlockRow = new TranslatedBlockRow(rawOriginalBlockRow);

                RCMigrator.MIGRATION_STORAGE.getTranslatedBlockRows().add(translatedBlockRow);

                rsCount++;
                logger.debug("Block = " + rawOriginalBlockRow + " translated to " + translatedBlockRow);
            }
            logger.info("Affected original blocks: " + rsCount);
            logger.info("Stored translated blocks: " + RCMigrator.MIGRATION_STORAGE.getTranslatedBlockRows().size());

            return true;

        } catch(SQLException e) {
            logger.error(e.getMessage());
        }

        return false;
    }

    public boolean getInventories() {

        try {

            logger.info("Connecting to inventories table");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(120);

            ResultSet rs = statement.executeQuery(SELECT_ALL_INVENTORIES);
            int rsCount = 0;
            while(rs.next()) {
                TranslatedInventoryRow translatedInventoryRow = new TranslatedInventoryRow(
                    rs.getString("player"),
                    rs.getInt("type"),
                    rs.getString("storage"),
                    rs.getString("armor"),
                    rs.getString("extra"),
                    rs.getString("effects"),
                    rs.getInt("xp"),
                    rs.getInt("lastused")
                );

                RCMigrator.MIGRATION_STORAGE.getTranslatedInventoryRows().add(translatedInventoryRow);

                rsCount++;
                logger.debug("Inventory translated to " + translatedInventoryRow);
            }
            logger.info("Affected original inventories: " + rsCount);
            logger.info("Stored translated inventories: " + RCMigrator.MIGRATION_STORAGE
                    .getTranslatedInventoryRows().size());

            return true;

        } catch(SQLException e) {
            logger.error(e.getMessage());
        }

        return false;
    }

    public boolean disconnect() {
        try {

            if(connection != null) {
                connection.close();
                logger.info("OriginalDB connection closed");
                return true;
            }

        } catch(SQLException e) {
            logger.error("Connection close failed: " + e.getMessage());
        }

        return false;
    }
}
