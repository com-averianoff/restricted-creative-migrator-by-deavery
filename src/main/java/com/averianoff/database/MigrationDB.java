package com.averianoff.database;

import com.averianoff.RCMigrator;
import com.averianoff.model.TranslatedBlockRow;
import com.averianoff.model.TranslatedInventoryRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class MigrationDB {

    private static Logger logger = LoggerFactory.getLogger(MigrationDB.class);
    private static String BLOCK_TABLE_NAME = "rc_blocks";
    private static String INVENTORY_TABLE_NAME = "rc_inventories";
    private static String CREATE_BLOCK_TABLE = "CREATE TABLE IF NOT EXISTS " + BLOCK_TABLE_NAME +
            " (`x` INT NOT NULL, `y` INT NOT NULL, `z` INT NOT NULL, `world` VARCHAR NOT NULL, " +
            "`chunk_x` INT NOT NULL, `chunk_z` INT NOT NULL, `owner` CHAR(36) NOT NULL, " +
            "`created` INT UNSIGNED NOT NULL, UNIQUE (`x`, `y`, `z`, `world`))";
    private static String CREATE_INVENTORY_TABLE = "CREATE TABLE IF NOT EXISTS " + INVENTORY_TABLE_NAME +
            " (`player` CHAR(36) NOT NULL, `type` TINYINT(1), `storage` VARCHAR, `armor` VARCHAR, " +
            "`extra` VARCHAR, `effects` VARCHAR, `xp` BIGINT, `last_used` BIGINT(11), UNIQUE (player))";
    private static String INSERT_BLOCK = "insert into " + BLOCK_TABLE_NAME + " values(?, ?, ?, ?, ?, ?, ?, ?)";
    private static String INSERT_INVENTORY = "insert into " + INVENTORY_TABLE_NAME + " values(?, ?, ?, ?, ?, ?, ?, ?)";
    private Connection connection;

    public MigrationDB() {
        this.connection = null;
    }

    public Connection connect() {
        logger.info("Migration db connecting...");
        connection = null;

        try {

            connection = DriverManager.getConnection("jdbc:sqlite:rc_migrated.db");
            logger.info("Migration db connected");

        } catch(SQLException e) {
            logger.error("if the error message is \"out of memory\", it probably means no database file is found: "
                    + e.getMessage());
        }

        return connection;
    }

    public void createStructure() {
        try {

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(27);

            logger.info("Start migration database structure preparing...");
            statement.executeUpdate(CREATE_BLOCK_TABLE);
            statement.executeUpdate(CREATE_INVENTORY_TABLE);
            logger.info("Migration database structure prepared");

        } catch(SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void insertBlocks() {
        try {

            logger.info("Blocks migration started...");
            long blocksMigrated = 0;

            while (RCMigrator.MIGRATION_STORAGE.getTranslatedBlockRows().size() > 0) {
                int storageSize = RCMigrator.MIGRATION_STORAGE.getTranslatedBlockRows().size();
                int batchSize = Math.min(storageSize, 27000);

                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_BLOCK);
                preparedStatement.setQueryTimeout(270);

                logger.info("Blocks migrated successful: " + blocksMigrated
                        + " | Next batch size: " + batchSize + " | Estimated: " + storageSize);

                while (batchSize > 0) {
                    storageSize--;
                    TranslatedBlockRow blockRow = RCMigrator.MIGRATION_STORAGE
                            .getTranslatedBlockRows().get(storageSize);

                    preparedStatement.setLong(1, blockRow.getX());
                    preparedStatement.setLong(2, blockRow.getY());
                    preparedStatement.setLong(3, blockRow.getZ());
                    preparedStatement.setString(4, blockRow.getWorld());
                    preparedStatement.setLong(5, blockRow.getChunkX());
                    preparedStatement.setLong(6, blockRow.getChunkZ());
                    preparedStatement.setString(7, blockRow.getOwner());
                    preparedStatement.setLong(8, blockRow.getCreated());
                    preparedStatement.addBatch();

                    RCMigrator.MIGRATION_STORAGE.getTranslatedBlockRows().remove(storageSize);
                    batchSize--;
                }
                blocksMigrated += preparedStatement.executeLargeBatch().length;
            }

            logger.info("Blocks migrated successful: " + blocksMigrated);

        } catch(SQLException e) {
            logger.error(e.getMessage());
        }

    }

    public void insertInventories() {
        try {

            logger.info("Inventories migration started...");
            long inventoriesMigrated = 0;

            while (RCMigrator.MIGRATION_STORAGE.getTranslatedInventoryRows().size() > 0) {
                int storageSize = RCMigrator.MIGRATION_STORAGE.getTranslatedInventoryRows().size();
                int batchSize = Math.min(storageSize, 27000);

                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INVENTORY);
                preparedStatement.setQueryTimeout(270);

                logger.info("Inventories migrated successful: " + inventoriesMigrated
                        + " | Next batch size: " + batchSize + " | Estimated: " + storageSize);

                while (batchSize > 0) {
                    storageSize--;
                    TranslatedInventoryRow inventoryRow = RCMigrator.MIGRATION_STORAGE
                            .getTranslatedInventoryRows().get(storageSize);

                    preparedStatement.setString(1, inventoryRow.getPlayer());
                    preparedStatement.setInt(2, inventoryRow.getType());
                    preparedStatement.setString(3, inventoryRow.getStorage());
                    preparedStatement.setString(4, inventoryRow.getArmor());
                    preparedStatement.setString(5, inventoryRow.getExtra());
                    preparedStatement.setString(6, inventoryRow.getEffects());
                    preparedStatement.setLong(7, inventoryRow.getXp());
                    preparedStatement.setLong(8, inventoryRow.getLastUsed());

                    preparedStatement.addBatch();
                    RCMigrator.MIGRATION_STORAGE.getTranslatedInventoryRows().remove(storageSize);
                    batchSize--;
                }
                inventoriesMigrated += preparedStatement.executeLargeBatch().length;
            }

            logger.info("Inventories migrated successful: " + inventoriesMigrated);

        } catch(SQLException e) {
            logger.error(e.getMessage());
        }

    }

    public boolean disconnect() {
        try
        {
            if(connection != null) {
                connection.close();
                logger.info("MigrationDB connection closed");
                return true;
            }
        }
        catch(SQLException e)
        {
            logger.error("Connection close failed: " + e.getMessage());
        }
        return false;
    }
}
