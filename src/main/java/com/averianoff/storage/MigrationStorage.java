package com.averianoff.storage;

import com.averianoff.model.TranslatedBlockRow;
import com.averianoff.model.TranslatedInventoryRow;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class MigrationStorage {
    private List<TranslatedBlockRow> translatedBlockRows = new ArrayList<>();
    private List<TranslatedInventoryRow> translatedInventoryRows = new ArrayList<>();
}
