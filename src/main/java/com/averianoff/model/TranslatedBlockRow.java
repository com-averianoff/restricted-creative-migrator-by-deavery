package com.averianoff.model;

import com.averianoff.RCMigrator;
import lombok.Data;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
@ToString
public class TranslatedBlockRow {

    private static Logger logger = LoggerFactory.getLogger(TranslatedBlockRow.class);

    private long x;
    private long y;
    private long z;
    private String world;
    private long chunkX;
    private long chunkZ;
    private String owner;
    private long created;

    public TranslatedBlockRow(String block) {
        String[] divided = block.split(";");
        if (divided.length != 4) logger.error("Broken block: " + block);
        world = divided[0];
        x = Long.parseLong(divided[1]);
        y = Long.parseLong(divided[2]);
        z = Long.parseLong(divided[3]);

        chunkX = (long) Math.floor(x/16.0);
        chunkZ = (long) Math.floor(z/16.0);

        owner = RCMigrator.BLOCK_OWNER;
        created = RCMigrator.CURRENT_TIME;
    }
}