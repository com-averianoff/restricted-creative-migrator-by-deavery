package com.averianoff.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class TranslatedInventoryRow {
    String player;
    int type;
    String storage;
    String armor;
    String extra;
    String effects;
    long xp;
    long lastUsed;
}
